#-------------------------------------------------------------------------------
# Name:        redisServerStartup.py
# Purpose:      redisServerStartup  stub
#
# Author:      hao_han
#
# Created:     05/09/2012
# Copyright:   (c) hao_han 2012
# Licence:     <BSM>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import os, sys
import redis
import json
import ctypes
import psycopg2, psycopg2.extras
import ConfigParser


file_object = open("/var/log/rum/redis-startup.log",'w')

def startup(redis_server_ip, redis_server_port, conn):

    try:
        rdb = redis.Redis(redis_server_ip, int(redis_server_port))
        file_object.write("Success to connect to redis server\n")
    except Exception, e:
        file_object.write("Failed to connect to redis server\n with exception: %s\n" % str(e))

    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    cursor.execute('select * from collector_db')
    collector_db = cursor.fetchall()
    #collector_list = []

    for collector_id, collector_ip, collector_name in collector_db:
        collector = {
                'collector_id' : collector_id,
                'collector_ip' : collector_ip,
                'collector_name' : collector_name,
                }

        cursor.execute('select * from service_db where collector_id = (%s)', (collector_id,))
        service_db = cursor.fetchall()

        for service_id, service_name, service_ip, service_port, service_type, service_url, performance_zero, performance_full, performance_lower, performance_upper, availability_lower, availability_upper, event_period, event_counts_warning, event_counts_critical, collector_id in service_db:
            service_info = {
                        'service_id'    : service_id,
                        'service_name'  : service_name,
                        'service_url'   : service_url,
                        'service_type'  : service_type,
                        'service_ip'    : service_ip,
                        'service_port'  : service_port
                      }

            #service_list.append(service)
            if rds.hexists('rum:service_map:collector_%d' % collector_id, service_id) :
                rdb.hdel('rum:service_map:collector_%d' % collector_id, service_id)
            rdb.hset('rum:service_map:collector_%d' % collector_id, str(service_id), json.dumps(service_info))

            service_thres = {
                            'service_id'            : service_id,
                            'event_period'          : event_period,
                            'event_counts_warning'  : event_counts_warning,
                            'event_counts_critical' : event_counts_critical,
                            'performance_zero'      : performance_zero,
                            'performance_full'      : performance_full,
                            'performance_lower'     : performance_lower,
                            'performance_upper'     : performance_upper,
                            'availability_lower'    : availability_lower,
                            'availability_upper'    : availability_upper
                      }
            #service_thres_list.append(service_thres)

            if rds.hexists('rum:service_thres_map:collector_%d' % collector_id, service_id) :
                rdb.hdel('rum:service_thres_map:collector_%d' % collector_id, service_id)
            rdb.hset('rum:service_thres_map:collector_%d' % collector_id, str(service_id), json.dumps(service_thres))

            event_responsetime_info = {
                        'start_time'    :   0,
                        'end_time'      :   0,
                        'event_counts'  :   0,
                        'response_time' :   0
                      }

            if not rds.hexists('rum:event_responsetime_map', service_id) :  #have no backup data
                rdb.hset('rum:event_responsetime_map', str(service_id), json.dumps(event_responsetime_info))

            event_availability_info = {
                        'start_time'    :   0,
                        'end_time'      :   0,
                        'event_counts'  :   0,
                        'availability'  :   0
                      }

            if not rds.hexists('rum:event_availability_map', service_id) :
                rdb.hset('rum:event_availability_map', str(service_id), json.dumps(event_availability_info))

            dumper_time_info = {
                        'last_hour'    :   0,
                        'last_day'     :   0
                      }
            cursor.execute("SELECT max(end_time) FROM hour_service_db WHERE service_id=%s" % str(service_id))
            last_time = cursor.fetchall()
            if last_time == [[None]] :
                dumper_time_info['last_hour'] = 0
            else :
                dumper_time_info['last_hour'] = last_time[0][0]

            cursor.execute('SELECT max(end_time) FROM day_service_db WHERE service_id=%s' % str(service_id))
            last_time = cursor.fetchall()
            if last_time == [[None]] :
                dumper_time_info['last_day'] = 0
            else :
                dumper_time_info['last_day'] = last_time[0][0]

            rdb.hset('rum:dumper_timerecords_map', str(service_id), json.dumps(dumper_time_info))

        if rdb.exists('rum:collector_map') :
            rdb.delete('rum:collector_map')
        rdb.hset('rum:collector_map', collector_id, json.dumps(collector))

    cursor.close()
    conn.close()

def main(argv = sys.argv):


    if len(argv) < 4 :
        file_object.write("Usage: %s redis_path redis_config_path global_config_path!" % argv[0])
        sys.exit(0)

    file_object.write("redis_path = %s\n" % argv[1])
    file_object.write("redis_config_path = %s\n" % argv[2])
    file_object.write("global_config_path = %s\n" % argv[3])

    redis_path = argv[1]
    redis_config_path = argv[2]
    try:
        binary_path = os.path.join(redis_path,'redis-server')
    except Exception, e:
        file_object.write("Failed to join %s with redis-server" % redis_path )

    file_object.write("redis_binary_path : %s\n" % binary_path )

    os.environ['LD_LIBRARY_PATH'] = '/usr/local/lib'
    return_status = os.system("%s %s" % (binary_path, redis_config_path))
    file_object.write("return_status: %d\n" % return_status)

    global_config_path = argv[3]

    try:

        #file_path = os.path.join(argv[1],'config','config.ini')
        #print "config_file_path = %s" % file_path

        config = ConfigParser.ConfigParser()
        config.read(global_config_path)
        db_host = config.get("Database","db_host")
        db_port = config.getint("Database","db_port")
        db_user = config.get("Database","db_user")
        db_password = config.get("Database","db_password")
        db_name = config.get("Database","db_name")

        redis_host = config.get("Redis","db_host")
        redis_port = config.getint("Redis","db_port")

        file_object.write("db_server = %s:%s\n" % (db_host,str(db_port)))
        file_object.write("redis_server = %s:%s\n" % (redis_host,str(redis_port)))

    except Exception, e:
        file_object.write("Failed to initilize from config.ini\nwith exception: %s\n" % str(e))


    try:
        #conn = selectdb(db_host, db_port, db_user, db_password, db_name)
        conn = psycopg2.connect(host=db_host, port=db_port, user=db_user,\
				password=db_password, database=db_name)
    except Exception, e:
        file_object.write("Failed to connect database\nwith exception: %s\n" % str(e))
        sys.exit(1)

    file_object.write("Success connect to database\n")
    startup(redis_host, redis_port, conn)

    file_object.write("Success to initilize redis server\n")

    file_object.close()

if __name__ == '__main__':
    main()
