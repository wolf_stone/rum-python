import ConfigParser
import os,sys
import commands
import tempfile

file_object = open("/var/log/dumper-startup.log",'w')

def getPid():
    cmd = "ps -ef | grep Dumper | awk '!(/grep/||/service/)'" 
    info = commands.getoutput(cmd)
    infos = info.split()
    if len(infos) > 1:
        return infos[1]
    else:
        return -1

def start(binary_path, lib_path, config_path):
    try:
        file_object.write("binary_path = %s\n" % binary_path)
        file_object.write("lib_path = %s\n" % lib_path)
        file_object.write("config_path = %s\n" % config_path)

        config = ConfigParser.ConfigParser()
        config.read(config_path)
        redis_server_ip = config.get("Redis", "db_host")
        redis_server_port = config.getint("Redis", "db_port")

        db_server_ip = config.get("Database", "db_host")
        db_server_port = config.getint("Database", "db_port")

        log_level = config.get("Logger", "log_level")

        file_object.write("log_level = %s\n" % log_level)
    
        if log_level == "WARNING" :
            glog_level = 1
        elif log_level == "ERROR" :
            glog_level = 2
        elif log_level == "CRITICAL" :
            glog_level = 3
        else :
            glog_level = 0

    except Exception, e:
        file_object.write("Failed to initilize from configuration\n")
        file_object.write("Reason: %s\n" % str(e))
        return


    os.environ['redis_server'] = redis_server_ip 
    os.environ['redis_port'] = str(redis_server_port)

    os.environ['db_server'] = db_server_ip 
    os.environ['db_port'] = str(db_server_port) 

    #os.environ['PATH'] = '../bin'
    os.environ['LD_LIBRARY_PATH'] = lib_path
    dumper_path = os.path.join(binary_path,'Dumper')


    try :
        os.system("GLOG_log_dir=/var/log/dumper.log GLOG_stderrthreshold=%d nohup %s  > /dev/null &" % (glog_level, dumper_path))
    except Exception, e:
        file_object.write("Failed to start Dumper with reason = %s\n" % str(e))
        file_object.close()
        sys.exit(0)

    pid = getPid()
    file_object.write("pid = %s\n" % pid)
    if pid < 0 :
        file_object.write("pid of Dumper does not exist")
        file_object.close()
        sys.exit(0)
        
    pid_file = open("/var/run/dumper.pid",'w')
    pid_file.write("%s" % pid)
    pid_file.close()

    file_object.close()

def main(argv = sys.argv):

    if len(argv) < 3 :
        file_object.write("Usage: %s binary_path lib_path config_path!\n" % argv[0])
        sys.exit(0)

    start(argv[1],argv[2],argv[3])

if __name__ == '__main__':
    main()

