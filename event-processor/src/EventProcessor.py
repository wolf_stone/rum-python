import os,sys
import thread, threading
import json,logging
import ConfigParser
from DatabaseClient import DatabaseClient
from RedisClient import RedisClient
from CommonModules import LogClient
from Computing import *
from Watcher import Watcher
import time
from redis.exceptions import ConnectionError
from decimal import Decimal, getcontext

class Eventprocessor(threading.Thread):

    def __init__(self, logger):
        threading.Thread.__init__(self)
        self.timeinterval = 3600

        self.redis_server = os.environ.get('redis_server')
        self.redis_port = int(os.environ.get('redis_port'))

        self.db_server = os.environ.get('db_server')
        self.db_port = int(os.environ.get('db_port'))
        self.db_user = os.environ.get('db_user')

        self.logger = logger

        self.logger.info("redis_server = %s:%d" %(self.redis_server,self.redis_port))
        self.logger.info("db_server = %s:%d" %(self.db_server,self.db_port))

        self.setName('EventProcessor '+ self.getName())
        self.logger.info("Event processor : %s is starting..." % self.getName())

        self.rdsobj = RedisClient(self.redis_server,self.redis_port,logger=self.logger)
        if self.rdsobj == None :
            self.errmsg = "failed to connect to redis server"
            self.logger.error(self.errmsg)
            return None


        self.dbobj = DatabaseClient(server_ip=self.db_server,server_port=self.db_port,logger=self.logger)
        if self.dbobj == None :
            self.errmsg = "failed to connect to database"
            self.logger.error(self.errmsg)
            return None

        self.logger.info("Event processor : %s now connected to the DataBase %s:%s " % (self.getName(),self.db_server,self.db_port))


    def start(self) :

        PubSub = self.rdsobj.pubSub()
        while True:
            try :
                PubSub.psubscribe("rum:pubsub*")
                self.logger.info("Event processor : %s start to subscribe..." % self.getName())
                for msg in PubSub.listen():
                    if  r"rum:pubsub_0_result:collector" in msg["channel"]:
                        #self.logger.debug("msg['channel'] is %s, msg['data'] is %s" % (msg["channel"],msg["data"]))
                        collector_id = int(msg["channel"].split('_')[-1])
                        real_info =  json.loads(msg["data"])
                        self.logger.debug("real_info = %s" % real_info)

                        service_id = real_info["service_id"]

                        service_info = self.rdsobj.pipeGetServiceInfo(collector_id,service_id)
                        #self.logger.debug("service_info = %s" % service_info)

                        ret = self.validCheck(real_info, collector_id)
                        if ret == -1 :
                            continue

                        if real_info and service_info:
                            perf_dict = realinfo_performance(real_info,service_info)
                        else:
                            self.logger.warning("Empty real_info or service_info!")
                            continue

                        if perf_dict :
                            SLA_dict = to_performanceSLA(perf_dict,self.logger)
                        else :
                            self.logger.warning("Empty perf_dict!")
                            continue

                        event_data = {}
                        event_data.setdefault('service_id',service_id)
                        event_data.setdefault('end_time',real_info['end_time'])
                        event_data.setdefault('service_name',service_info['service_name'])
                        event_data.setdefault('collector_id',collector_id)


                        self.sla_event(event_data, perf_dict, SLA_dict)
                        #TODO: mail send

                    elif  r"rum:pubsub_event:collector" in msg["channel"]:
                        self.logger.info("msg['channel'] is %s, msg['data'] is %s" % (msg["channel"],msg["data"]))
                        collector_id = int(msg["channel"].split('_')[-1])
                        event_data =  json.loads(msg["data"])
                        service_id = event_data['service_id']
                        service_info = self.rdsobj.pipeGetServiceInfo(collector_id,service_id)

                        event_time = event_data['event_time']
                        if event_data.has_key('event_time') :
                            del event_data['event_time']
                        event_data['start_time'] = event_time
                        event_data['end_time'] = event_time

                        event_data['severity_level'] = "Warning".upper()
                        event_data['service_name'] = service_info['service_name']
                        event_data['event_counts'] = 1

                        self.dbobj.addEventInfo(event_data, 'NETWORK')

            except Exception, e:
                self.logger.error("error to pubsub with reason = %s" % e)
                PubSub = self.rdsobj.pubSub()
######
    def validCheck(self, real_info, collector_id) :
        start_time = real_info['start_time']
        end_time = real_info['end_time']
        current = (int)(time.time())
        if start_time > end_time :
            self.logger.warning("start_time=%s great than end_time=%s, bad realinfo"
                                % (start_time,end_time))
            return -1

        if abs(start_time - current) > self.timeinterval :
            self.logger.warning("time interval between start_time=%s and current=%s \
                                more than one hour, please check time in collector_%d"
                                % (start_time,current,collector_id))
            return -1

        if abs(end_time - current) > self.timeinterval :
            self.logger.warning("time interval between end_time=%s and current=%s \
                                more than one hour, please check time in collector_%d"
                                % (end_time,current,collector_id))
            return -1

        return 0
######

    def setResponsetimeInfo(self, service_id, start_time,
                            end_time, event_counts,
                            response_time) :
        responsetime_info = {}
        responsetime_info['start_time'] = start_time
        responsetime_info['end_time'] = end_time
        responsetime_info['event_counts'] = event_counts
        responsetime_info['response_time'] = str(response_time)

        self.logger.debug("updated responsetime_info = %s" % responsetime_info)
        self.rdsobj.setResponsetimeInfo(service_id, responsetime_info)

    def setAvailabilityInfo(self, service_id, start_time,
                            end_time, event_counts,
                            availability) :
        availability_info = {}
        availability_info['start_time'] = start_time
        availability_info['end_time'] = end_time
        availability_info['event_counts'] = event_counts
        availability_info['availability'] = str(availability)

        self.logger.debug("updated availability_info = %s" % availability_info)
        self.rdsobj.setAvailabilityInfo(service_id, availability_info)

    def sla_responsetime(self, event_data, perf_dict, SLA_dict) :
        if SLA_dict['event_period'] == 0 :
            self.logger.debug("event_period for service=%s is setted to 0", event_data['service_name'])
            return

        current_time = event_data["end_time"]
        self.logger.debug("current_time=%s" % time.ctime(current_time))

        service_id = perf_dict['service_id']
        self.logger.debug("event_name = %s" % event_data["event_name"])
        responsetime_info = self.rdsobj.getResponsetimeInfo(service_id)
        self.logger.debug("original responsetime_info = %s" % responsetime_info)
        if responsetime_info['start_time'] == 0 :
            self.logger.info("It is the first SLA event for service_id=%d" % service_id)

            responsetime_info['start_time'] = current_time
            responsetime_info['end_time'] = current_time
            responsetime_info['event_counts'] = 1
            responsetime_info['response_time'] = str(perf_dict['response_time'])

            self.logger.debug("updated responsetime_info = %s" % responsetime_info)
            self.rdsobj.setResponsetimeInfo(service_id, responsetime_info)
            return

        if current_time - responsetime_info['start_time'] < SLA_dict['event_period'] :
            self.logger.debug("Inside the event_period")
            responsetime_info['end_time'] = current_time
            responsetime_info['event_counts'] = responsetime_info['event_counts'] + 1
            responsetime_info['response_time'] = str(Decimal(responsetime_info['response_time'])
                                                    + perf_dict['response_time'])
            self.logger.debug("updated responsetime_info = %s" % responsetime_info)
            self.rdsobj.setResponsetimeInfo(service_id, responsetime_info)
            return

        if current_time - responsetime_info['start_time'] == SLA_dict['event_period'] :
            self.logger.debug("It is the ending of the event_period")

            getcontext().prec = 3
            event_data['start_time'] = responsetime_info['start_time']
            event_data['end_time'] = current_time
            event_data['event_counts'] = responsetime_info['event_counts'] + 1
            if event_data['event_counts'] > 0 :
                event_data['response_time'] = Decimal(str(responsetime_info['response_time'])) + perf_dict['response_time']
                event_data['response_time'] = event_data['response_time'] \
                                            / Decimal(event_data['event_counts'])
            else :
                event_data['response_time'] = Decimal(0)

            if event_data['response_time'] < Decimal(str(SLA_dict['performance_upper'])) :
                self.logger.debug("updated response_time = %s" % event_data['response_time'])
                self.logger.debug("performance_upper = %s" % SLA_dict['performance_upper'])
                self.logger.debug("the new responsetime is not SLA event anymore")
                self.setResponsetimeInfo(service_id,current_time,
                                        current_time,0,0)
                return

            event_data['performance_lower'] = SLA_dict['performance_lower']
            event_data['performance_upper'] = SLA_dict['performance_upper']
            event_data['event_period'] = SLA_dict['event_period']
            event_data['event_counts_warning'] = SLA_dict['event_counts_warning']
            event_data['event_counts_critical'] = SLA_dict['event_counts_critical']

            if event_data['event_counts'] >= SLA_dict['event_counts_warning'] and event_data['event_counts'] < SLA_dict['event_counts_critical'] :
                event_data['severity_level'] = 'Warning'.upper()

            elif event_data['event_counts'] >= SLA_dict['event_counts_critical'] :
                event_data['severity_level'] = 'Critical'.upper()

            else :
                self.logger.debug("event_counts less than event_counts_warning")
                self.setResponsetimeInfo(service_id,current_time,
                                        current_time,0,0)
                return

            self.logger.debug("event_data = %s\n" % event_data)

            ret = self.dbobj.addEventInfo(event_data, 'SLA_RESPT')
            if ret < 0 :
                self.logger.warning("failed to add event_data into event_db with reason = %s" % self.dbobj.getErrMsg())
            else :
                self.logger.debug("success to add event_data into event_db")

            self.setResponsetimeInfo(service_id,current_time,
                                    current_time,0,0)

        else :
            self.logger.debug("current time is out of the ending of the event_period")

            getcontext().prec = 3
            event_data['start_time'] = responsetime_info['start_time']
            event_data['end_time'] = responsetime_info['end_time']
            event_data['event_counts'] = responsetime_info['event_counts']

            if event_data['event_counts'] > 0 :
                event_data['response_time'] = Decimal(str(responsetime_info['response_time'])) \
                                            / Decimal(event_data['event_counts'])
            else :
                event_data['response_time'] = Decimal(0)

            if event_data['response_time'] < Decimal(str(SLA_dict['performance_upper'])) :
                self.logger.debug("updated response_time = %s" % event_data['response_time'])
                self.logger.debug("performance_upper = %s" % SLA_dict['performance_upper'])
                self.logger.debug("the new responsetime is not SLA event anymore")
                self.setResponsetimeInfo(service_id,current_time,
                                        current_time,1,
                                        perf_dict['response_time'])
                return

            if event_data['event_counts'] >= SLA_dict['event_counts_warning'] and event_data['event_counts'] < SLA_dict['event_counts_critical'] :
                event_data['severity_level'] = 'Warning'.upper()
                self.logger.debug("set severity_level to WARNING")
            elif event_data['event_counts'] >= SLA_dict['event_counts_critical'] :
                event_data['severity_level'] = 'Critical'.upper()
                self.logger.debug("set severity_level to CRITICAL")
            else :
                self.logger.debug("event_counts less than event_counts_warning")
                self.setResponsetimeInfo(service_id,current_time,
                                        current_time,1,
                                        perf_dict['response_time'])
                return

            event_data['performance_lower'] = SLA_dict['performance_lower']
            event_data['performance_upper'] = SLA_dict['performance_upper']
            event_data['event_period'] = SLA_dict['event_period']
            event_data['event_counts_warning'] = SLA_dict['event_counts_warning']
            event_data['event_counts_critical'] = SLA_dict['event_counts_critical']

            self.logger.debug("event_data = %s\n" % event_data)

            ret = self.dbobj.addEventInfo(event_data, 'SLA_RESPT')
            if ret < 0 :
                self.logger.warning("failed to add event_data into event_db with reason = %s" % self.dbobj.getErrMsg())
            else :
                self.logger.debug("success to add event_data into event_db")

            self.setResponsetimeInfo(service_id,current_time,
                                    current_time,1,
                                    perf_dict['response_time'])

######
    def sla_availability(self, event_data, perf_dict, SLA_dict) :
        if SLA_dict['event_period'] == 0 :
            self.logger.debug("event_period is setted to 0")
            return

        current_time = event_data["end_time"]
        self.logger.debug("current_time=%s" % time.ctime(current_time))

        service_id = perf_dict['service_id']

        self.logger.debug("event_name = %s" % event_data["event_name"])
        availability_info = self.rdsobj.getAvailabilityInfo(service_id)
        self.logger.debug("original availability_info = %s" % availability_info)

        if availability_info['start_time'] == 0 :
            self.logger.info("It is the first SLA event for service_id=%d" % service_id)
            availability_info['start_time'] = current_time
            availability_info['end_time'] = current_time
            availability_info['event_counts'] = 1
            availability_info['availability'] = str(perf_dict['availability'])

            self.logger.debug("updated availability_info = %s" % availability_info)
            self.rdsobj.setAvailabilityInfo(service_id, availability_info)
            return

        if current_time - availability_info['start_time'] < SLA_dict['event_period'] :
            self.logger.debug("Inside the event_period")
            availability_info['end_time'] = current_time
            availability_info['event_counts'] = availability_info['event_counts'] + 1
            availability_info['availability'] = str(Decimal(availability_info['availability'])
                                                    + perf_dict['availability'])
            self.logger.debug("updated availability_info = %s" % availability_info)
            self.rdsobj.setAvailabilityInfo(service_id, availability_info)
            return

        if current_time - availability_info['start_time'] == SLA_dict['event_period'] :
            self.logger.debug("It is the ending of the event_period")

            getcontext().prec = 2
            event_data['start_time'] = availability_info['start_time']
            event_data['end_time'] = current_time
            event_data['event_counts'] = availability_info['event_counts'] + 1

            if event_data['event_counts'] > 0 :
                event_data['availability'] = Decimal(str(availability_info['availability'])) + perf_dict['availability']
                event_data['availability'] = event_data['availability'] / Decimal(event_data['event_counts'])
            else :
                event_data['availability'] = Decimal(0)

            if event_data['availability'] > Decimal(str(SLA_dict['availability_lower'])) :
                self.logger.debug("updated availability = %s" % event_data['availability'])
                self.logger.debug("availability_lower = %s" % SLA_dict['availability_lower'])
                self.logger.debug("the new availability is not SLA event anymore")
                self.setAvailabilityInfo(service_id,current_time,
                                        current_time,0,0)
                return

            if event_data['event_counts'] >= SLA_dict['event_counts_warning'] and event_data['event_counts'] < SLA_dict['event_counts_critical'] :
                event_data['severity_level'] = 'Warning'.upper()
            elif event_data['event_counts'] >= SLA_dict['event_counts_critical'] :
                event_data['severity_level'] = 'Critical'.upper()
            else :
                self.logger.debug("event_counts less than event_counts_warning")
                self.setAvailabilityInfo(service_id,current_time,
                                        current_time,0,0)
                return

            event_data['availability_lower'] = SLA_dict['availability_lower']
            event_data['availability_upper'] = SLA_dict['availability_upper']
            event_data['event_period'] = SLA_dict['event_period']
            event_data['event_counts_warning'] = SLA_dict['event_counts_warning']
            event_data['event_counts_critical'] = SLA_dict['event_counts_critical']

            self.logger.debug("event_data = %s\n" % event_data)

            ret = self.dbobj.addEventInfo(event_data, 'SLA_AVAI')
            if ret < 0 :
                self.logger.warning("failed to add event_data into event_db with reason = %s" % self.dbobj.getErrMsg())
            else :
                self.logger.debug("success to add event_data into event_db")

            self.setAvailabilityInfo(service_id,current_time,
                                    current_time,0,0)

        else :
            self.logger.debug("current time is out of the ending of the event_period")

            getcontext().prec = 2
            event_data['start_time'] = availability_info['start_time']
            event_data['end_time'] = availability_info['end_time']
            event_data['event_counts'] = availability_info['event_counts']
            if event_data['event_counts'] > 0 :
                event_data['availability'] = Decimal(str(availability_info['availability'])) \
                                        / Decimal(event_data['event_counts'])
            else :
                event_data['availability'] = Decimal(0)

            if event_data['availability'] > Decimal(str(SLA_dict['availability_lower'])) :
                self.logger.debug("updated availability = %s" % event_data['availability'])
                self.logger.debug("availability_lower = %s" % SLA_dict['availability_lower'])
                self.logger.debug("the new availability is not SLA event anymore")
                self.setAvailabilityInfo(service_id,current_time,
                                        current_time,1,
                                        perf_dict['availability'])
                return

            if event_data['event_counts'] >= SLA_dict['event_counts_warning'] and event_data['event_counts'] < SLA_dict['event_counts_critical'] :
                event_data['severity_level'] = 'Warning'.upper()
            elif event_data['event_counts'] >= SLA_dict['event_counts_critical'] :
                event_data['severity_level'] = 'Critical'.upper()
            else :
                self.logger.debug("event_counts less than event_counts_warning")
                self.setAvailabilityInfo(service_id,current_time,
                                        current_time,1,
                                        perf_dict['availability'])
                return

            event_data['availability_lower'] = SLA_dict['availability_lower']
            event_data['availability_upper'] = SLA_dict['availability_upper']
            event_data['event_period'] = SLA_dict['event_period']
            event_data['event_counts_warning'] = SLA_dict['event_counts_warning']
            event_data['event_counts_critical'] = SLA_dict['event_counts_critical']

            self.logger.debug("event_data = %s\n" % event_data)


            ret = self.dbobj.addEventInfo(event_data, 'SLA_AVAI')
            if ret < 0 :
                self.logger.warning("failed to add event_data into event_db with reason = %s" % self.dbobj.getErrMsg())
            else :
                self.logger.debug("success to add event_data into event_db")

            self.setAvailabilityInfo(service_id,current_time,
                                    current_time,1,
                                    perf_dict['availability'])

    #create event and add to db
    def sla_event(self, event_data, perf_dict, SLA_dict) :
        event_data["event_type"] = "SLA"
        responsetime_SLA = SLA_dict['responsetime_SLA']
        availability_SLA = SLA_dict['availability_SLA']

        if responsetime_SLA == 'RED' :
            self.logger.debug("response time branch")
            self.logger.debug("perf_dict = %s" % perf_dict)
            self.logger.debug("SLA_dict = %s" % SLA_dict)
            self.logger.debug("event_data = %s" % event_data)

            event_data["event_name"] = "SLA:100"
            self.sla_responsetime(event_data, perf_dict, SLA_dict)

        if availability_SLA == 'RED' :
            self.logger.debug("availability branch")
            self.logger.debug("perf_dict = %s" % perf_dict)
            self.logger.debug("SLA_dict = %s" % SLA_dict)
            self.logger.debug("event_data = %s" % event_data)

            event_data["event_name"] = "SLA:101"
            self.sla_availability(event_data, perf_dict, SLA_dict)

def main():

    log_level = os.environ.get('log_level')
    log_path = os.environ.get('log_path')
    log_size = int(os.environ.get('log_size'))
    log_count = int(os.environ.get('log_count'))
    logger = LogClient(log_path, "event-processor", log_level, log_size, log_count)

    logger.debug("log_size = %s" % log_size)
    logger.debug("log_count = %s" % log_count)

    Watcher()
    logger.info("Watcher object created!")
    eventprocesser = Eventprocessor(logger)
    if not eventprocesser :     #init failed
        logger.error("Failed to create eventprocessor object")
        sys.exit(1)
    else :
        logger.info("Success to create eventprocessor object")

    eventprocesser.start()


if __name__ == '__main__':
    main()

