# encoding=UTF-8
"""
Created on 2012-3-28
@author: 永刚
"""

from CommonModules import LogClient
import psycopg2, psycopg2.extras
import time


class DatabaseClient:
    def __init__(self, server_ip = "127.0.0.1", server_port = 5432, user = 'postgres',password='', logger = None, db='cmdb'):
        self.server_ip = server_ip
        self.server_port = server_port
        self.db_user = user
        self.db_password = password

        self.errmsg=""
        self.conn = None
        if logger == None :
            self.logger = LogClient("db.log")
        else :
            self.logger  = logger

        try:
            self.conn = psycopg2.connect(database = db, host=server_ip, port=server_port, user=user, password=password)
        except Exception, e:
            self.logger.error("failed to initilize database")
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return None
#######

    def getErrMsg(self) :
        return self.errmsg

#######

    def isConnected(self) :
        try :
            pid = self.conn.get_backend_pid()
            return True
        except Exception, e:
            self.logger.info("conn object have been destroyed")
            self.logger.info("with reason: %s" % str(e))
            return False

    def confirmConnected(self) :
        if self.isConnected() :
            return True
        else :
            while True:
                self.logger.warning("conn object disconnnected")
                try :
                    self.conn = psycopg2.connect(database = "cmdb", host=self.server_ip, port=self.server_port, user=self.db_user, password=self.db_password)
                    return True

                except Exception, e:
                    self.logger.error("Failed to reconnect DB")
                    self.errmsg = str(e)
                    self.logger.error("with reason = %s" % self.errmsg)
                    time.sleep(2)
                    continue

######

    def addEventInfo(self, event_info, type):

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return -1

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)


        try :

            if type == 'SLA_RESPT' :
                cmd_string = "INSERT INTO event_db(start_time,end_time,event_name,event_type    \
                            ,severity_level,event_counts,service_id,response_time               \
                            ,performance_lower,performance_upper,event_period                   \
                            ,event_counts_warning,event_counts_critical                         \
                            ,collector_id)                                                      \
                            VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"

                cursor.execute( cmd_string,(event_info['start_time'],event_info['end_time']     \
                            ,event_info['event_name'],event_info['event_type']                  \
                            ,event_info['severity_level'],event_info['event_counts']            \
                            ,event_info['service_id'],event_info['response_time']               \
                            ,event_info['performance_lower'],event_info['performance_upper']    \
                            ,event_info['event_period'],event_info['event_counts_warning']      \
                            ,event_info['event_counts_critical'],event_info['collector_id']) )

            elif type == 'SLA_AVAI' :
                cmd_string = "INSERT INTO event_db(start_time,end_time,event_name,event_type    \
                            ,severity_level,event_counts,service_id,availability                \
                            ,availability_lower,availability_upper,event_period                 \
                            ,event_counts_warning,event_counts_critical                         \
                            ,collector_id)                                                      \
                            VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"

                cursor.execute( cmd_string,(event_info['start_time'],event_info['end_time']     \
                            ,event_info['event_name'],event_info['event_type']                  \
                            ,event_info['severity_level'],event_info['event_counts']            \
                            ,event_info['service_id'],event_info['availability']                \
                            ,event_info['availability_lower'],event_info['availability_upper']  \
                            ,event_info['event_period'],event_info['event_counts_warning']      \
                            ,event_info['event_counts_critical'],event_info['collector_id']) )

            else :
                cmd_string = "INSERT INTO event_db(start_time,end_time,event_name,event_type    \
                            ,severity_level,event_counts,service_id,collector_id)               \
                            VALUES(%s,%s,%s,%s,%s,%s,%s,%s);"

                cursor.execute( cmd_string,(event_info['start_time'],event_info['end_time']     \
                            ,event_info['event_name'],event_info['event_type']                  \
                            ,event_info['severity_level'],event_info['event_counts']            \
                            ,event_info['service_id'],event_info['collector_id']) )

            self.conn.commit()
            cursor.close()
            return 0

        except Exception, e:
            cursor.close()
            errMsg = "[addEventInfo failed:"
            errMsg = errMsg + "event_data=%s" + str(event_info)
            self.logger.error(errMsg)
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return -1
######

    def __del__(self) :
        if self.conn != None :
            self.conn.close()
