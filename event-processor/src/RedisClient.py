import redis
import json
import time
from CommonModules import LogClient
from redis.exceptions import ConnectionError

class RedisClient:
    def __init__(self, server_ip = "127.0.0.1", server_port = 6379,logger=None ):
        self.errmsg = ""
        self.server_ip = server_ip
        self.server_port = server_port

        if logger == None :
            self.logger = LogClient("redis.log")
        else :
            self.logger  = logger

        try:
            #rds = redis.Redis(server_ip, server_port)
            #rds = redis.StrictRedis(server_ip, server_port, db=0)
            #self.subObj = rds.pubsub()

            self.pool = redis.ConnectionPool(host=server_ip, port=server_port, db=0)

        except Exception, e:
            self.logger.error("failed connect to redis server with ip=%s and port = %d" %(server_ip,server_port))
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return None

    def getErrMsg(self) :
        return self.errmsg
######

    def pubSub(self) :
        while True:
            try :
                rds = redis.Redis(connection_pool=self.pool)
                pubsub = rds.pubsub()
                rds.dbsize()
                self.logger.info("pubsub object created")
                return pubsub

            except Exception, e :
                self.logger.error("Redis failed to create pubsub object")
                self.errmsg = str(e)
                self.logger.error("with reason = %s" % self.errmsg)
                time.sleep(1)

######

    def pipeGetServiceInfo(self, collector_id, service_id = None) :

        service_dict = {}
        thres_dict = {}
        service_info = {}

        try:
            rds = redis.Redis(connection_pool=self.pool)
            pipe = rds.pipeline()

            if service_id == None :
                pipe.hgetall('rum:service_map:collector_%d' % collector_id)
                pipe.hgetall('rum:service_thres_map:collector_%d' % collector_id)
                output = pipe.execute()
                service_dict = output[0]
                thres_dict = output[1]
                for key in service_dict :
                    service_info[int(key)] = (json.loads(service_dict[key]))
                    service_info[int(key)].update(json.loads(thres_dict[key]))
            else :
                pipe.hget('rum:service_map:collector_%d' % collector_id, service_id)
                pipe.hget('rum:service_thres_map:collector_%d' % collector_id, service_id)
                output = pipe.execute()
                service_dict = output[0]
                thres_dict = output[1]
                service_info = (json.loads(service_dict))
                service_info.update(json.loads(thres_dict))

            return service_info

        except Exception, e:
            errmsg = "Failed to pipe get service list: "
            if service_id != None :
                errmsg = errmsg + "\twith collector_id=%d, service_id=%d" % (collector_id,service_id)
            else :
                errmsg = errmsg + "\twith collector_id=%d" % collector_id
            self.logger.warning(errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

            return None
######

    def getResponsetimeInfo(self, service_id) :

        try:
            rds = redis.Redis(connection_pool=self.pool)
            responsetime_info = json.loads(rds.hget('rum:event_responsetime_map', service_id))
            return responsetime_info 

        except Exception, e:
            errmsg = "Failed to get responsetime info from event_responsetime_map"
            errmsg = errmsg + "\twith service_id=%d", service_id
            self.logger.warning(errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

    def setResponsetimeInfo(self, service_id, responsetime_info) :

        if not responsetime_info :
            self.logger.warning("responsetime_info is empty")
            return
            
        try:
            rds = redis.Redis(connection_pool=self.pool)
            rds.hset('rum:event_responsetime_map', service_id, json.dumps(responsetime_info))
            return True

        except Exception, e:
            errmsg = "Failed to set responsetime info"
            errmsg = errmsg + "\twith responsetime info=%s" % responsetime_info
            self.logger.warning(errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)
######

    def getAvailabilityInfo(self, service_id) :

        try:
            rds = redis.Redis(connection_pool=self.pool)
            availability_info = json.loads(rds.hget('rum:event_availability_map', service_id))
            return availability_info

        except Exception, e:
            errmsg = "Failed to get availability info from event_availability_map"
            errmsg = errmsg + "\twith service_id=%d", service_id
            self.logger.warning(errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

            return None


    def setAvailabilityInfo(self, service_id, availability_info) :

        if not availability_info :
            self.logger.warning("availability_info is empty")
            return
            
        try:
            rds = redis.Redis(connection_pool=self.pool)
            rds.hset('rum:event_availability_map', service_id, json.dumps(availability_info))
            return True

        except Exception, e:
            errmsg = "Failed to set availability info"
            errmsg = errmsg + "\twith availability info=%s" % availability_info
            self.logger.warning(errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)
######
    def __del__(self) :
        self.pool.disconnect()
        self.logger.info("close redisclient object")
