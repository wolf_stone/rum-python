from decimal import Decimal, getcontext

def init_perfdict(serviceinfo) :     

    perf_dict = {}
    perf_dict['service_name'] = serviceinfo['service_name']
    perf_dict['service_id'] = serviceinfo['service_id']
    perf_dict['event_period'] = serviceinfo['event_period'] 
    perf_dict['event_counts_warning'] = serviceinfo['event_counts_warning'] 
    perf_dict['event_counts_critical'] = serviceinfo['event_counts_critical'] 
    perf_dict['performance_zero'] = serviceinfo['performance_zero']
    perf_dict['performance_full'] = serviceinfo['performance_full']
    perf_dict['performance_lower'] = serviceinfo['performance_lower']
    perf_dict['performance_upper'] = serviceinfo['performance_upper']
    perf_dict['availability_lower'] = serviceinfo['availability_lower']
    perf_dict['availability_upper'] = serviceinfo['availability_upper']

    return perf_dict


def realinfo_performance(realinfo, serviceinfo) :

	perf_dict = init_perfdict(serviceinfo)

	if not perf_dict :              #empty
		return None

	if not realinfo :               #realinfo is empty
		perf_dict['response_time'] = 0
		perf_dict['availability'] = 0
		return perf_dict


	if serviceinfo['service_type'] == 'HTTP' :

		if realinfo['success_connections'] == 0 or realinfo['request_num'] == 0 :
			perf_dict['response_time'] = 0
		else :
			if realinfo['retry_num'] == 0 :
				realinfo['retry_num'] = 1
			perf_dict['response_time'] = float(realinfo['retry_time'])/realinfo['retry_num'] + float(realinfo['connection_time'])/realinfo['success_connections'] + float(realinfo['network_time2first_buf'] + realinfo['server_time2first_buf'] + realinfo['download_time']) / realinfo['request_num']

			getcontext().prec = 3
			perf_dict['response_time'] = Decimal(str(round(perf_dict['response_time']/1000,3))) #us to ms

	else :                                                  #TCP
		if realinfo['success_connections'] == 0 :
			perf_dict['response_time'] = 0
		else :
			getcontext().prec = 3
			perf_dict['response_time'] = float(realinfo['tcp_performance']) / realinfo['success_connections']
			perf_dict['response_time'] = Decimal(str(round(perf_dict['response_time']/1000,3)))  #us to ms

	if realinfo['service_hits'] == 0 :
		perf_dict['availability'] = 0
	else :
		perf_dict['availability'] = (float(realinfo['success_hits']) / realinfo['service_hits']) * 100
		perf_dict['availability'] = Decimal(str(round(perf_dict['availability'],2)))

	return perf_dict


def to_performanceSLA(perf_dict,logger=None) :

    responsetime_SLA = 'UNDEF'
    availability_SLA = 'UNDEF'


    response_time = Decimal(str(perf_dict['response_time']))
    perf_zero = Decimal(str(perf_dict['performance_zero']))
    perf_full = Decimal(str(perf_dict['performance_full']))
    perf_lower = Decimal(str(perf_dict['performance_lower']))
    perf_upper = Decimal(str(perf_dict['performance_upper']))

    #[a,b)
    if response_time == Decimal(str(0)) :
        responsetime_SLA = 'UNDEF'
    elif response_time >= perf_zero and response_time < perf_lower :
        responsetime_SLA = 'GREEN'
    elif response_time >= perf_lower and response_time < perf_upper :
        responsetime_SLA = 'YELLOW'
    elif response_time >= perf_upper :
        responsetime_SLA = 'RED'


    availability = Decimal(str(perf_dict['availability']))
    avai_lower = Decimal(str(perf_dict['availability_lower']))
    avai_upper = Decimal(str(perf_dict['availability_upper']))

    if availability == Decimal(str(0)) :
        availability_SLA = 'UNDEF'
    elif availability <= avai_lower :
        availability_SLA = 'RED'
    elif availability <= avai_upper :
        availability_SLA = 'YELLOW'
    else :
        availability_SLA = 'GREEN'

    #if logger :
    #    logger.debug("perf_dict in SLA computing = %s" % perf_dict)
    SLA_dict = {'responsetime_SLA':responsetime_SLA, 'availability_SLA':availability_SLA}
    SLA_dict['performance_lower'] = perf_dict['performance_lower'] 
    SLA_dict['performance_upper'] = perf_dict['performance_upper'] 
    SLA_dict['availability_lower'] = perf_dict['availability_lower'] 
    SLA_dict['availability_upper'] = perf_dict['availability_upper'] 
    SLA_dict['event_period'] = perf_dict['event_period'] 
    SLA_dict['event_counts_warning'] = perf_dict['event_counts_warning'] 
    SLA_dict['event_counts_critical'] = perf_dict['event_counts_critical'] 

    return SLA_dict

