import os, time
import datetime
import logging
#from logging.handlers import TimedRotatingFileHandler
from logging.handlers import RotatingFileHandler

class LogClient:

    def __init__(self, log_dir, file_name_prefix, level="WARNING", LOGSIZE = 10, LOGCOUNT=3):

        self.log_dir = log_dir
        self.file_name_prefix = file_name_prefix
        self._mkdirs()
        #self.baseFilename = "%s.%s.log" % (os.path.join(self.log_dir, file_name_prefix),time.strftime("%Y%m%d"))
        self.baseFilename = "%s.log" % (os.path.join(self.log_dir, file_name_prefix))

        '''
        self.hdlr = TimedRotatingFileHandler(
                self.baseFilename,
                when='midnight', interval=1,
                backupCount=5, encoding=None)
	    '''
        if not LOGSIZE :
            LOGSIZE = 10

        LOGSIZE = LOGSIZE * 1024 * 1024
        self.hdlr = RotatingFileHandler(self.baseFilename,
                            mode='a',
                            maxBytes=LOGSIZE,
                            #maxBytes=10*1024*1024,
                            backupCount=LOGCOUNT)

        level_dict = {}
        level_dict["NOTSET"] = logging.NOTSET
        level_dict["DEBUG"] = logging.DEBUG
        level_dict["INFO"] = logging.INFO
        level_dict["WARNING"] = logging.WARNING
        level_dict["ERROR"] = logging.ERROR
        level_dict["CRITICAL"] = logging.CRITICAL


        self.logger = logging.getLogger()
        self.logger.setLevel(level_dict[level])
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        self.hdlr.setFormatter(formatter)
        self.logger.addHandler(self.hdlr)

    def _mkdirs(self):
        if not os.path.exists(self.log_dir):
            try:
                os.makedirs(self.log_dir)
            except Exception,e:
                print str(e)


    def debug(self, msg):
        self.logger.addHandler(self.hdlr)
        self.logger.debug(msg)
        self.logger.removeHandler(self.hdlr)

    def info(self, msg):
        self.logger.addHandler(self.hdlr)
        self.logger.info(msg)
        self.logger.removeHandler(self.hdlr)

    def warning(self, msg):
        self.logger.addHandler(self.hdlr)
        self.logger.warning(msg)
        self.logger.removeHandler(self.hdlr)

    def error(self, msg):
        self.logger.addHandler(self.hdlr)
        self.logger.error(msg)
        self.logger.removeHandler(self.hdlr)


def display_service( logger, service_item ) :

    logger.debug("---------------Start to Display Service_t---------------")

    logger.debug("service_id = %d" % service_item['service_id'])
    logger.debug("service_name = %s" % service_item['service_name'])
    logger.debug("service_ip = %s" % service_item['service_ip'])
    logger.debug("service_port = %s" % service_item['service_port'])
    logger.debug("app_type = %s" % service_item['app_type'])
    logger.debug("url = %s" % service_item['url'])
    logger.debug("perfor_zero = %f" % service_item['perfor_zero'])
    logger.debug("perfor_full = %f" % service_item['perfor_full'])
    logger.debug("perfor_lower = %f" % service_item['perfor_lower'])
    logger.debug("perfor_upper = %f" % service_item['perfor_upper'])
    logger.debug("availa_lower = %f" % service_item['availa_lower'])
    logger.debug("availa_upper = %f" % service_item['availa_upper'])
    logger.debug("collector_id = %d" % service_item['collector_id'])

    logger.debug("---------------End of Display Service_t---------------")

