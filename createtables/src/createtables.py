#-------------------------------------------------------------------------------
# Name:        createdb.py
# Purpose:     create database table stub
#
# Author:      hao_han
#
# Created:     03/Apr/2012
# Copyright:   (c) hao_han 2012
# Licence:     <Beilizhu>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import os
import psycopg2, psycopg2.extras
from ctypes import *

def createdb(host, port=5432, user='postgres',database='cmdb'):
    #conn = psycopg2.connect(host=host, port=port, user=user, password=password,           database=database)
    conn = psycopg2.connect(host=host, port=port, user=user, database=database)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    cmd_string = "DROP SEQUENCE IF EXISTS authentication_db_userid_seq CASCADE;"
    cursor.execute( cmd_string )
    cmd_string = "CREATE SEQUENCE authentication_db_userid_seq;"
    cursor.execute( cmd_string )

    cmd_string = "DROP TABLE IF EXISTS authentication_db;"
    cursor.execute( cmd_string )
    cmd_string = "CREATE TABLE authentication_db(               \
            user_id int4 NOT NULL DEFAULT NEXTVAL('authentication_db_userid_seq') primary key,          \
            user_name varchar(40) NOT NULL,                     \
            password varchar(40) NOT NULL,                      \
            user_group varchar(40) NOT NULL                     \
            );"
    cursor.execute( cmd_string )

    cmd_string = "DROP SEQUENCE IF EXISTS collector_db_collectorid_seq CASCADE;"
    cursor.execute( cmd_string )
    cmd_string = "CREATE SEQUENCE collector_db_collectorid_seq;"
    cursor.execute( cmd_string )

    cmd_string = "DROP TABLE IF EXISTS collector_db CASCADE;"
    cursor.execute( cmd_string )
    cmd_string = "CREATE TABLE collector_db(                    \
            collector_id int4 NOT NULL DEFAULT NEXTVAL('collector_db_collectorid_seq') primary key,     \
            collector_ip varchar(40) NOT NULL,                  \
            collector_name varchar(40) NOT NULL                 \
            );"
    cursor.execute( cmd_string )

    """
    cmd_string = "DROP SEQUENCE IF EXISTS service_group_db_groupid_seq CASCADE;"
    cursor.execute( cmd_string )
    cmd_string = "CREATE SEQUENCE service_group_db_groupid_seq;"
    cursor.execute( cmd_string )

    cmd_string = "DROP TABLE IF EXISTS service_group_db CASCADE;"
    cursor.execute( cmd_string )
    cmd_string = "CREATE TABLE service_group_db (       \
            group_id int4 NOT NULL DEFAULT NEXTVAL('service_group_db_groupid_seq') primary key,     \
            group_name varchar(80) NOT NULL,            \
            description varchar(100) NOT NULL,          \
            collector_id int4 references collector_db(collector_id)     \
            );"
    cursor.execute( cmd_string )
    """

    cmd_string = "DROP SEQUENCE IF EXISTS service_db_serviceid_seq CASCADE;"
    cursor.execute( cmd_string )
    cmd_string = "CREATE SEQUENCE service_db_serviceid_seq;"
    cursor.execute( cmd_string )

    cmd_string = "DROP TABLE IF EXISTS service_db CASCADE;"
    cursor.execute( cmd_string )
    cmd_string = "CREATE TABLE service_db(              \
            service_id int4 NOT NULL DEFAULT NEXTVAL('service_db_serviceid_seq') primary key,  \
            service_name varchar(128) NOT NULL,         \
            service_ip varchar(128) NOT NULL,           \
            service_port varchar(16) NOT NULL,          \
            service_type varchar(32) NOT NULL,          \
            service_url varchar(1024),                  \
            performance_zero float8 NOT NULL,           \
            performance_full float8 NOT NULL,           \
            performance_lower float8 NOT NULL,          \
            performance_upper float8 NOT NULL,          \
            availability_lower float8 NOT NULL,         \
            availability_upper float8 NOT NULL,         \
            event_period int4 NOT NULL,                 \
            event_counts_warning int4 NOT NULL,         \
            event_counts_critical int4 NOT NULL,        \
            collector_id int4 references collector_db(collector_id) ON DELETE CASCADE NOT NULL  \
            );"
    cursor.execute( cmd_string )

    cmd_string = "INSERT INTO authentication_db(user_name,password,user_group)      \
            VALUES('admin','admin','admin');"
    cursor.execute( cmd_string )

    cmd_string = "INSERT INTO authentication_db(user_name,password,user_group)      \
            VALUES('user','user','user');"
    cursor.execute( cmd_string )

    cmd_string = "DROP TABLE IF EXISTS event_db;"
    cursor.execute( cmd_string )

    '''
    cmd_string = "CREATE TABLE event_db(                    \
                  service_id int4 references service_db(service_id),                \
                  event_time int8 NOT NULL,                 \
                  event_name varchar(128) NOT NULL,         \
                  event_type varchar(32) NOT NULL,          \
                  severity_level varchar(32) NOT NULL,      \
                  collector_id int4 references collector_db(collector_id),          \
                  message  varchar(512)                     \
                  );"
    cursor.execute( cmd_string )
    '''
    cmd_string = "CREATE TABLE event_db(                \
                start_time int8 NOT NULL,               \
                end_time int8 NOT NULL,                 \
                event_name varchar(128) NOT NULL,       \
                event_type varchar(32) NOT NULL,        \
                severity_level varchar(32) NOT NULL,    \
                event_counts int8 NOT NULL,             \
                service_id int4 references service_db(service_id) ON DELETE CASCADE NOT NULL,       \
                response_time  float8,          \
                performance_lower float8,       \
                performance_upper float8,       \
                availability float8,            \
                availability_lower float8,      \
                availability_upper float8,      \
                event_period int4,              \
                event_counts_warning int4,      \
                event_counts_critical int4,     \
                collector_id int4 references collector_db(collector_id) ON DELETE CASCADE NOT NULL  \
                );"
    cursor.execute( cmd_string )

    cmd_string = "DROP TABLE IF EXISTS day_service_db;"
    cursor.execute( cmd_string )
    cmd_string = "CREATE TABLE day_service_db(              \
            service_id int4 references service_db(service_id) ON DELETE CASCADE NOT NULL,           \
            start_time int8 NOT NULL,                       \
            end_time int8 NOT NULL,                         \
            service_hits int8 NOT NULL,                     \
            success_hits int8 NOT NULL,                     \
            success_connections int8 NOT NULL,              \
            connection_time int8 NOT NULL,                  \
            network_time2first_buf int8 NOT NULL,           \
            server_time2first_buf int8 NOT NULL,            \
            download_time int8 NOT NULL,                    \
            retry_time int8 NOT NULL,                       \
            retry_num int8 NOT NULL,                        \
            request_num int8 NOT NULL,                      \
            page_size int8 NOT NULL,                        \
            service_throughput int8 NOT NULL,               \
            tcp_performance int8 NOT NULL,                  \
            collector_id int4 references collector_db(collector_id) ON DELETE CASCADE NOT NULL      \
            );"
    cursor.execute( cmd_string )

    cmd_string = "DROP TABLE IF EXISTS hour_service_db;"
    cursor.execute( cmd_string )
    cmd_string = "CREATE TABLE hour_service_db(             \
            service_id int4 references service_db(service_id) ON DELETE CASCADE NOT NULL,           \
            start_time int8 NOT NULL,                       \
            end_time int8 NOT NULL,                         \
            service_hits int8 NOT NULL,                     \
            success_hits int8 NOT NULL,                     \
            success_connections int8 NOT NULL,              \
            connection_time int8 NOT NULL,                  \
            network_time2first_buf int8 NOT NULL,           \
            server_time2first_buf int8 NOT NULL,            \
            download_time int8 NOT NULL,                    \
            retry_time int8 NOT NULL,                       \
            retry_num int8 NOT NULL,                        \
            request_num int8 NOT NULL,                      \
            page_size int8 NOT NULL,                        \
            service_throughput int8 NOT NULL,               \
            tcp_performance int8 NOT NULL,                  \
            collector_id int4 references collector_db(collector_id) ON DELETE CASCADE NOT NULL      \
            );"
    cursor.execute( cmd_string )

    cmd_string = "DROP TABLE IF EXISTS minute_service_db;"
    cursor.execute( cmd_string )
    cmd_string = "CREATE TABLE minute_service_db(           \
            service_id int4 references service_db(service_id) ON DELETE CASCADE NOT NULL,           \
            start_time int8 NOT NULL,                       \
            end_time int8 NOT NULL,                         \
            service_hits int8 NOT NULL,                     \
            success_hits int8 NOT NULL,                     \
            success_connections int8 NOT NULL,              \
            connection_time int8 NOT NULL,                  \
            network_time2first_buf int8 NOT NULL,           \
            server_time2first_buf int8 NOT NULL,            \
            download_time int8 NOT NULL,                    \
            retry_time int8 NOT NULL,                       \
            retry_num int8 NOT NULL,                        \
            request_num int8 NOT NULL,                      \
            page_size int8 NOT NULL,                        \
            service_throughput int8 NOT NULL,               \
            tcp_performance int8 NOT NULL,                  \
            collector_id int4 references collector_db(collector_id) ON DELETE CASCADE NOT NULL      \
            );"
    cursor.execute( cmd_string )

    conn.commit()
    cursor.close()
    conn.close()

def main():
    #output = os.popen("which postgres").read()
    #src_string = "bin" + os.sep + "postgres"
    #output = output.replace( src_string , "lib" )
    #output = "/usr/local/pgsql/lib"
    #cmd_string = "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:" + output

    #if os.system(cmd_string) == 0 :
    #    print "Success add path %s to LD_LIBRARY_PATH\n" % output

    #createdb(host='192.168.137.253', port=5432, user='postgres',password='', database='cmdb')
    createdb(host='127.0.0.1')

if __name__ == '__main__':
    main()
